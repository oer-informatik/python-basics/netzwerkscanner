import subprocess
import ipaddress
import socket
import sys


def get_own_ip_address():
    connection_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    connection_socket.connect(("8.8.8.8", 80))
    own_ip_address = str(connection_socket.getsockname()[0])+"/31"
    connection_socket.close()
    return own_ip_address


def config_subprocess():
    info = subprocess.STARTUPINFO()
    info.dwFlags |= subprocess.STARTF_USESHOWWINDOW
    info.wShowWindow = subprocess.SW_HIDE
    return info


if len(sys.argv) > 1:
    network_address = sys.argv[1]
else:
    text = "Bitte geben Sie eine IP-Adresse mit Subnet ein \n"
    text += "(zum Beispiel 192.168.1.0/24): "
    network_address = input(text)

if network_address == "":
    network_address = get_own_ip_address()

print("Das folgende Netzwerk wird durchsucht: ", network_address)

ip_network = ipaddress.ip_network(network_address, strict=False)
network_host_list = list(ip_network.hosts())

print("Dieses Netzwerk besteht aus den folgenden Hosts:")
for host in network_host_list:
    print(host)

for i in range(len(network_host_list)):
    print("")
    print("### --- ", str(network_host_list[i]), "wird untersucht --- ###")
    command = 'ping -n 1 -w 500 ' + str(network_host_list[i])
    output = subprocess.Popen(command, stdout=subprocess.PIPE).communicate()[0]
    print(output.decode('cp850', errors='backslashreplace').strip())
